package ru.inno.game.dto;

/**
 * 25.03.2021
 * GameIntro
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
// информация об игре
public class StatisticDto{
    private Long gameId;
    private String playerOneName;
    private Integer playerOnePoints;
    private Integer playerOneMaxWinsCount;
    private String playerTwoName;
    private Integer playerTwoPoints;
    private Integer playerTwoMaxWinsCount;
    private Long gameTime;
    private String winner;


    public StatisticDto() {
    }

    public String getWinner() {
        return winner;
    }

    public void setWinner(String winner) {
        this.winner = winner;
    }

    public Long getGameId() {
        return gameId;
    }

    public void setGameId(Long gameId) {
        this.gameId = gameId;
    }

    public String getPlayerOneName() {
        return playerOneName;
    }

    public void setPlayerOneName(String playerOneName) {
        this.playerOneName = playerOneName;
    }

    public Integer getPlayerOnePoints() {
        return playerOnePoints;
    }

    public void setPlayerOnePoints(Integer playerOnePoints) {
        this.playerOnePoints = playerOnePoints;
    }

    public Integer getPlayerOneMaxWinsCount() {
        return playerOneMaxWinsCount;
    }

    public void setPlayerOneMaxWinsCount(Integer playerOneMaxWinsCount) {
        this.playerOneMaxWinsCount = playerOneMaxWinsCount;
    }

    public String getPlayerTwoName() {
        return playerTwoName;
    }

    public void setPlayerTwoName(String playerTwoName) {
        this.playerTwoName = playerTwoName;
    }

    public Integer getPlayerTwoPoints() {
        return playerTwoPoints;
    }

    public void setPlayerTwoPoints(Integer playerTwoPoints) {
        this.playerTwoPoints = playerTwoPoints;
    }

    public Integer getPlayerTwoMaxWinsCount() {
        return playerTwoMaxWinsCount;
    }

    public void setPlayerTwoMaxWinsCount(Integer playerTwoMaxWinsCount) {
        this.playerTwoMaxWinsCount = playerTwoMaxWinsCount;
    }

    public Long getGameTime() {
        return gameTime;
    }

    public void setGameTime(Long gameTime) {
        this.gameTime = gameTime;
    }

    @Override
    public String toString() {
        return ("Игра с ID = " + gameId + "\nИгрок 1: " + playerOneName + ", попаданий - " + playerOnePoints + ", всего очков - " + playerOneMaxWinsCount + "\nИгрок 2: " + playerTwoName + ", попаданий - " + playerTwoPoints + ", всего очков - " + playerTwoMaxWinsCount + "\nИгра длилась: " + gameTime + " секунд\n" + "Победитель: " + winner + "\n");
    }
}
