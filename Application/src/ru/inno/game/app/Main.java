package ru.inno.game.app;

import ru.inno.game.dto.StatisticDto;
import ru.inno.game.repository.*;
import ru.inno.game.services.GameService;
import ru.inno.game.services.GameServiceImpl;

import java.util.Random;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        PlayersRepository playersRepository = new PlayersRepositoryMapImpl();
        GamesRepository gamesRepository = new GamesRepositoryListImpl();
        ShotsRepository shotsRepository = new ShotsRepositoryListImpl();
        GameService gameService = new GameServiceImpl(playersRepository, gamesRepository, shotsRepository);

        Scanner scanner = new Scanner(System.in);

        String first = scanner.nextLine();
        String second = scanner.nextLine();
        Random random = new Random();

        int j=0;
        while (j < 3) {
            Long gameId = gameService.startGame("127.0.0.1", "127.0.0.2", first, second);
            String shooter = first;
            String target = second;
            int i = 0;
            int shooterScore = 0;
            int targetScore = 0;
            while (i < 10) {

                System.out.println(shooter + " делайте выстрел в " + target);
                scanner.nextLine();

                int success = random.nextInt(2);

                if (success == 0) {
                    System.out.println("Успешно!");
                    gameService.shot(gameId, shooter, target);
                } else {
                    System.out.println("Промах!");
                }

                String temp = shooter;
                shooter = target;
                target = temp;
                i++;
            }
            StatisticDto statistic = gameService.finishGame(gameId);
            System.out.println(statistic);
            j++;
        }
    }
}